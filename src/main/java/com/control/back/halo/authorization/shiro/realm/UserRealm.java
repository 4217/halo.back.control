package com.control.back.halo.authorization.shiro.realm;

import static java.util.stream.Collectors.toSet;

import java.util.Date;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.control.back.halo.basic.entity.User;
import com.control.back.halo.basic.utils.UserUtils;
import com.control.back.halo.manage.entity.Admin;
import com.control.back.halo.manage.entity.Function;
import com.control.back.halo.manage.entity.Role;
import com.control.back.halo.manage.service.IAdminService;

/**
 * 
 * @author zhaohuiliang
 *
 */
@Component
public class UserRealm extends AuthorizingRealm {

    @Autowired
    private IAdminService adminService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String username = (String) principals.getPrimaryPrincipal();
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        Admin admin = adminService.findByUsername(username);
        Set<String> shiroPermissions = admin.getRoles().stream()
                .map(Role::getFunctions).flatMap(Set::stream)
                .map(Function::getShiroPermission).collect(toSet());
        authorizationInfo.setStringPermissions(shiroPermissions);
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String username = (String) token.getPrincipal();
        Admin admin = adminService.findByUsername(username);
        String password = new String((char[]) token.getCredentials());

        if (admin == null) { throw new UnknownAccountException(); }
        User user = admin.getUser();
        // 账号不存在
        if (user == null) { throw new UnknownAccountException("账号或密码不正确"); }
        // 密码错误
        if (!StringUtils.equals(password, user.getPassword())) { throw new IncorrectCredentialsException("账号或密码不正确"); }
        // 账号锁定
        // if (admin.getLocked() == 0) { throw new
        // LockedAccountException("账号已被锁定,请联系管理员"); }

        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user, password, getName());

        admin.setLoginTime(new Date());
        adminService.update(admin);

        // 把user信息设置到session中
        UserUtils.setCurrentUser(user);

        return info;
    }

    @Override
    public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
        super.clearCachedAuthorizationInfo(principals);
    }

    @Override
    public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
        super.clearCachedAuthenticationInfo(principals);
    }

    @Override
    public void clearCache(PrincipalCollection principals) {
        super.clearCache(principals);
    }

    public void clearAllCachedAuthorizationInfo() {
        getAuthorizationCache().clear();
    }

    public void clearAllCachedAuthenticationInfo() {
        getAuthenticationCache().clear();
    }

    public void clearAllCache() {
        clearAllCachedAuthenticationInfo();
        clearAllCachedAuthorizationInfo();
    }

}

package com.control.back.halo.manage.freemarker;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import freemarker.template.Configuration;
import freemarker.template.TemplateModelException;

@Component
public class FreemarkerConfig {

    @Autowired
    private Configuration            configuration;
    @Autowired
    private BreadcrumbModelDirective breadcrumbModelDirective;

    @PostConstruct
    public void setSharedVariable() throws TemplateModelException {
        configuration.setSharedVariable("breadcrumb", breadcrumbModelDirective);
    }

}
